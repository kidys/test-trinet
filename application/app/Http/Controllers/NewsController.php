<?php

namespace App\Http\Controllers;

use App\News;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\Request;
use Illuminate\View\View;

/**
 * Class NewsController
 * @package App\Http\Controllers
 */
class NewsController extends Controller
{
    /**
     * @return Factory|View
     */
    public function index()
    {
        $news = News::paginate(env('PAGINATE_COUNT_NEWS', 3));
        return view('news.index', compact('news'));
    }

    /**
     * @param Request $request
     * @return Factory|View
     */
    public function show(Request $request)
    {
        $slug = $request->route('slug');

        $one_news = News::where('slug', $slug)->firstOrFail();
        return view('news.show', compact('one_news'));
    }
}
