<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\News;
use Illuminate\Http\Response;
use Illuminate\Support\Str;

class NewsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $news = News::paginate(env('PAGINATE_COUNT_NEWS_ADMIN', 2));
        return view('admin.news.index', compact('news'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('admin.news.form');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'title' => [
                'required',
                'min:3',
                'max:128',
                'unique:news,title,' . $request->get('title')
            ],
            'description' => 'required',
            'content' => 'required',
            'timestamp_publish' => 'required'
        ], [
            'timestamp_publish.required' => 'The date publish field is required.'
        ]);

        $news = new News([
            'title' => $request->get('title'),
            'slug' => Str::slug($request->get('title')),
            'description' => $request->get('description'),
            'content' => $request->get('content'),
            'timestamp_publish' => $request->get('timestamp_publish')
        ]);
        $news->save();

        return redirect()->route('admin.index');
    }

    /**
     * Display the specified resource.
     *
     * @param Request $request
     * @return Response
     */
    public function show(Request $request)
    {
        $title = $request->route('title');

        $one_news = News::where('title', $title)->firstOrFail();
        return view('admin.news.show', compact('one_news'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        $news = News::find($id);
        return view('admin.news.edit', compact('news'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'title' => [
                'required',
                'min:3',
                'max:128'
            ],
            'description' => 'required',
            'content' => 'required',
            'timestamp_publish' => 'required'
        ], [
            'timestamp_publish.required' => 'The date publish field is required.'
        ]);

        $news = News::find($id);
        $news->title = $request->get('title');
        $news->slug = Str::slug($news->title);
        $news->description = $request->get('description');
        $news->content = $request->get('content');
        $news->timestamp_publish = $request->get('timestamp_publish');

        $news->save();

        return redirect()->route('admin.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        $news = News::find($id);
        $news->delete();

        return redirect()->route('admin.index');
    }
}
