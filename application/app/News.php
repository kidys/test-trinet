<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;

/**
 * Class News
 * @package App
 *
 * @method static $this where($column, $operator = null, $value = null, $boolean = 'and')
 * @method Model|Builder firstOrFail($columns = ['*'])
 * @method static LengthAwarePaginator paginate($perPage = null, $columns = ['*'], $pageName = 'page', $page = null)
 */
class News extends Model
{
    /**
     * @var array
     */
    protected $fillable = [
        'title',
        'slug',
        'timestamp_publish',
        'description',
        'content'
    ];

    /**
     * @return array
     */
    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'title'
            ]
        ];
    }
}
