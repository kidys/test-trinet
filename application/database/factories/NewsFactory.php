<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */
use App\News;
use Faker\Generator as Faker;
use Illuminate\Support\Str;

$factory->define(News::class, function (Faker $faker) {
    $timestamps = $faker->dateTimeBetween('-9 days','-8 days');
    $generator_title = $faker->text(rand(64, 128));

    return [
        'title' => $generator_title,
        'slug' => Str::slug($generator_title),
        'timestamp_publish' => $faker->dateTimeBetween('-7 days', '-1 days'),
        'description' => $faker->text(rand(128, 256)),
        'content' => $faker->text(rand(512, 1024)),
        'created_at' => $timestamps,
        'updated_at' => $timestamps
    ];
});
