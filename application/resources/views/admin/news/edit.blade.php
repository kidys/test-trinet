@extends('base')

@section('main')
<div class="row">
    <div class="col-12">
        <h1>Edit news!</h1>
    </div>
    <hr class="w-100"/>
    <div class="col-12">
    @if ($errors->any())
        <div class="alert alert-danger">
            <ul class="form-errors">
                @foreach ($errors->all() as $error)
                <li>{{$error}}</li>
                @endforeach
            </ul>
        </div>
    @endif
    </div>
    <div class="col-12">
        <form method="post" action="{{Illuminate\Support\Facades\URL::route('news.update', ['id' => $news->id])}}">
            @method('PATCH')
            @csrf
            <div class="form-group">
                <label for="first_name">Title:</label>
                <input type="text" class="form-control" name="title" value="{{$news->title}}"/>
            </div>
            <div class="form-group">
                <label for="last_name">Description:</label>
                <textarea class="form-control" rows="5" name="description">{{$news->description}}</textarea>
            </div>
            <div class="form-group">
                <label for="last_name">Content:</label>
                <textarea class="form-control" rows="8" name="content">{{$news->content}}</textarea>
            </div>
            <div class="form-group">
                <label for="first_name">Date publish:</label>
                <input type="hidden" value="{{date('Y-m-d', strtotime($news->created_at))}}"/>
                <input type="date" class="form-control" name="timestamp_publish" value="{{date('Y-m-d', strtotime($news->timestamp_publish))}}"/>
            </div>
    </div>
    <hr class="w-100"/>
    <div class="col-12">
        <div class="row justify-content-between align-items-center">
            <div class="col-6">
                <a href="{{Illuminate\Support\Facades\URL::route('admin.index')}}"><< Back to list news</a>
            </div>
            <div class="col-6 text-right">
                <button type="submit" class="btn btn-success btn-sm">Update</button>
            </div>
        </div>
        </form>
    </div>
</div>
@endsection