@php
/**
 * @var App\News $current_news
 */
@endphp

@extends('base')

@section('main')
<div class="row">
    <div class="col-12">
        <h1 class="text-center text-md-left">Admin list is news! <small><a href="{{Illuminate\Support\Facades\URL::route('admin.create')}}" class="btn btn-success btn-sm">Create news?!</a></small></h1>
    </div>
    <hr class="w-100"/>
    <div class="col-12 card-list">
        <div class="card-table">
            <div class="row card-header text-center">
                <div class="col-12 col-md-1"><strong>#ID</strong></div>
                <div class="col-12 col-md-6"><strong>Title/Description</strong></div>
                <div class="col-12 col-md-2"><strong>Date publish</strong></div>
                <div class="col-12 col-md-3"></div>
            </div>
            @foreach($news as $current_news)
            <div class="row card-body">
                <div class="col-12 col-md-1 text-center">{{$current_news->id}}</div>
                <div class="col-12 col-md-6">
                    <h5>{{$current_news->title}}</h5>
                    <br/>
                    <div>{{$current_news->description}}</div>
                </div>
                <div class="col-12 col-md-2 text-center">{{date('j F, Y', strtotime($current_news->timestamp_publish))}}</div>
                <div class="col-12 col-md-3">
                    <div class="row justify-content-center btn-group_kidys">
                        <a href="{{Illuminate\Support\Facades\URL::route('admin.edit', ['id' => $current_news->id])}}" class="btn btn-primary btn-sm">Edit</a>
                        <form method="post" action="{{Illuminate\Support\Facades\URL::route('admin.destroy', ['id' => $current_news->id])}}">
                            @csrf
                            @method('DELETE')
                            <button type="submit" class="btn btn-danger btn-sm">Delete</button>
                        </form>
                    </div>
                </div>
            </div>
            @endforeach
        </div>
    </div>
    <hr class="w-100"/>
    <div class="col-12">
        {!! $news->links('vendor.pagination.simple-bootstrap-4') !!}
    </div>
</div>
@endsection