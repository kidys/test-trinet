@extends('base')

@section('main')
<div class="row">
    <div class="col-12">
        <h1>Show one news!</h1>
    </div>
    <hr class="w-100"/>
    <div class="col-12">
        <div class="card">
            <h3 class="card-header">{{$one_news->title}}</h3>
            <div class="card-body">
                <div class="news-item__description">{{$one_news->description}}</div>
                <hr class="w-100"/>
                <div class="news-item__description">{{$one_news->content}}</div>
            </div>
            <div class="card-footer text-right">
                <strong>created is:</strong>&nbsp;{{date('j F, Y', strtotime($one_news->timestamp_publish))}}
            </div>
        </div>
    </div>
    <hr class="w-100"/>
    <div class="col-12">
        <a href="{{Illuminate\Support\Facades\URL::route('news.all')}}"><< Back to list news</a>
    </div>
</div>
@endsection