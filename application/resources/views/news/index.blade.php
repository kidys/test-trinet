@php
/**
 * @var App\News $current_news
 */
@endphp

@extends('base')

@section('main')
<div class="row">
    <div class="col-12">
        <h1>List all news!</h1>
    </div>
    <hr class="w-100"/>
    <div class="col-12 card-list">
        @foreach($news as $current_news)
            <div class="card card_kidys_theme">
                <h3 class="card-header">
                    <a href="{{Illuminate\Support\Facades\URL::route('news.one', ['slug' => $current_news->slug])}}">{{$current_news->title}}</a>
                </h3>
                <div class="card-body">
                    <div class="news-item__description">{{$current_news->description}}</div>
                </div>
                <div class="card-footer text-right">
                    <strong>created is:</strong>&nbsp;{{date('j F, Y', strtotime($current_news->timestamp_publish))}}
                </div>
            </div>
        @endforeach
    </div>
    <hr class="w-100"/>
    <div class="col-12">
        {!! $news->links('vendor.pagination.bootstrap-4') !!}
    </div>
</div>
@endsection