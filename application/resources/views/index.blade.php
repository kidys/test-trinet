@extends('base')

@section('main')
<div class="row">
    <div class="col-12">
        <h1>Main page project! Select page bottom?!</h1>
    </div>
    <div class="col-12">
        <ul>
            <li><a href="{{Illuminate\Support\Facades\URL::route('news.all')}}">All news</a> (frontend)</li>
            <li><a href="{{Illuminate\Support\Facades\URL::route('admin.index')}}">Admin panel</a> (backend)</li>
        </ul>
    </div>
</div>
@endsection