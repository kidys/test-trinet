<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Route;

Route::get('/', function () {
    return view('index');
});

Route::group(['prefix' => 'admin'], function() {
    Route::get('/', 'Admin\NewsController@index')->name('admin.index');

    Route::resource('news', 'Admin\NewsController')->names([
        'create' => 'admin.create',
        'edit' => 'admin.edit',
        'destroy' => 'admin.destroy'
    ]);
});

Route::get('/news', ['uses' => 'NewsController@index'])->name('news.all');
Route::get('/news/{slug}', ['uses' => 'NewsController@show'])->name('news.one');
